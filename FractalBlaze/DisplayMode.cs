﻿namespace FractalBlaze
{
    public enum DisplayMode
    {
        None,
        SquareOfCurrent,
        ChainOfSquares,
        Julia
    }
}
