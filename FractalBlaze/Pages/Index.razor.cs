﻿using Blazor.Extensions;
using Blazor.Extensions.Canvas.Canvas2D;
using MathNet.Spatial.Euclidean;
using MathNet.Spatial.Units;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading;
using System.Threading.Tasks;

namespace FractalBlaze.Pages
{
    public partial class Index
    {
        private Canvas2DContext context, axesContext;
        private BECanvasComponent canvas, axesCanvas;

        private CancellationTokenSource token;
        private int maxIterations = 20, scale = 300, juliaPower = 2;
        private long canvasTop, canvasLeft, canvasHeight, canvasWidth, drawTime;
        private DisplayMode mode = DisplayMode.None;
        private bool drawR1Circle = true, drawAxes = true, swapModifier, doDraw, busy;
        private Complex selectedPoint = Complex.Zero, currentPoint;
        private (long x, long y) Zero => (canvasWidth / 2, canvasHeight / 2);
        private System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        private List<LineSegment2D> canvasSides = new List<LineSegment2D>(4);

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await UpdateBounds(true);
            }

            context = await canvas.CreateCanvas2DAsync();
            sw.Restart();

            if (doDraw)
            {
                switch (mode)
                {
                    case DisplayMode.SquareOfCurrent:
                        await context.ClearRectAsync(0, 0, canvasWidth, canvasHeight);
                        await DrawSquareDot(context);
                        break;
                    case DisplayMode.ChainOfSquares:
                        await context.ClearRectAsync(0, 0, canvasWidth, canvasHeight);
                        await DrawSquareChain(context);
                        break;
                }
            }
            sw.Stop();
            drawTime = sw.ElapsedMilliseconds;
        }

        private async Task UpdateBounds(bool resize)
        {
            if (resize) await js.InvokeVoidAsync("resizeCanvas");
            var bounds = await js.InvokeAsync<Dictionary<string, double>>("getCanvasBounds");
            canvasTop = (long)bounds["top"];
            canvasLeft = (long)bounds["left"];
            canvasHeight = (long)bounds["height"];
            canvasWidth = (long)bounds["width"];
            var points = new[] {
            new Point2D(0, 0), new Point2D(canvasWidth, 0),
            new Point2D(canvasWidth, canvasHeight), new Point2D(0, canvasHeight) };
            canvasSides.Clear();
            points.Aggregate((f, s) =>
            {
                canvasSides.Add(new LineSegment2D(f, s));
                return s;
            });
        }

        private void Click(MouseEventArgs args)
        {
            if (args.Button == 2)
                selectedPoint = currentPoint;
        }

        private void MouseMove(MouseEventArgs args) =>
            currentPoint = ToCardinalCoords(args.ClientX, args.ClientY);

        private void SetDisplayMode(DisplayMode dm)
        {
            mode = dm == mode ? DisplayMode.None : dm;
            token?.Cancel();
        }

        private (double x, double y) ToCanvasCoords(Complex c) =>
            ToCanvasCoords(c.Real, c.Imaginary);

        private (double x, double y) ToCanvasCoords(double x, double y) =>
            (x * scale + canvasWidth / 2, -y * scale + canvasHeight / 2);

        private Complex ToCardinalCoords(double x, double y) => new Complex(
            (x - canvasLeft - canvasWidth / 2) / scale, (canvasHeight / 2 - y + canvasTop) / scale);

        private async Task DrawSquareDot(Canvas2DContext ctx)
        {
            var newCoord = ToCanvasCoords(Complex.Pow(currentPoint, 2));
            await DrawDot(ctx, newCoord.x, newCoord.y);
        }

        private async Task DrawSquareChain(Canvas2DContext ctx)
        {
            var current = swapModifier ? selectedPoint : currentPoint;
            var points = new List<(double x, double y)>(maxIterations) { ToCanvasCoords(current) };

            for (int i = 0; i < maxIterations; i++)
            {
                current = Complex.Pow(current, 2) + (swapModifier ? currentPoint : selectedPoint);
                if (double.IsInfinity(current.Magnitude)) break;
                else points.Add(ToCanvasCoords(current));
            }

            if (!points.Any()) return;
            bool singlePath = points.All(IsWitinBounds);

            await ctx.SetFillStyleAsync("transparent");
            await ctx.SetStrokeStyleAsync("silver");
            await ctx.SetLineWidthAsync(1);
            if (singlePath)
            {
                await ctx.BeginPathAsync();
                await ctx.MoveToAsync(points[0].x, points[0].y);
            }

            for (int i = 0; i < points.Count - 1; i++)
            {
                if (singlePath)
                {
                    await ctx.LineToAsync(points[i + 1].x, points[i + 1].y);
                }
                else
                {
                    try
                    {
                        if (AllWithinBounds(points[1], points[i + 1]))
                        {
                            await ctx.BeginPathAsync();
                            await ctx.MoveToAsync(points[i].x, points[i].y);
                            await ctx.LineToAsync(points[i + 1].x, points[i + 1].y);
                            await ctx.StrokeAsync();
                        }
                        else if (TryGetIntersectionCoords(points[i], points[i + 1],
                          out var p1x, out var p1y, out var p2x, out var p2y))
                        {
                            await ctx.BeginPathAsync();
                            await ctx.MoveToAsync(p1x, p1y);
                            await ctx.LineToAsync(p2x, p2y);
                            await ctx.StrokeAsync();
                        }
                    }
                    catch { }
                }
            }
            if (singlePath) await ctx.StrokeAsync();

            try
            {
                foreach (var p in points) await DrawDot(ctx, p.x, p.y);
            }
            catch { }
        }

        private async Task DrawJulia(Canvas2DContext ctx)
        {
            busy = true;
            var currentSelected = selectedPoint;
            int currentPower = juliaPower;
            token = new CancellationTokenSource();
            await Task.Delay(5);
            await ctx.ClearRectAsync(0, 0, canvasWidth, canvasHeight);
            var topLeft = ToCanvasCoords(-2, 1.5);
            int r = 255, g = 140, b = 0;
            string getInterpolated(int c, int i, double max) =>
                $"{((int)(c + (255 - c) * i / max)):X}";
            var colors = Enumerable.Range(0, maxIterations - 1)
                .Select(x => $"#FF{getInterpolated(g, x, maxIterations)}{getInterpolated(b, x, maxIterations)}")
                .Reverse().ToArray();
            for (double x = topLeft.x; x < topLeft.x + 4 * scale; x++)
            {
                for (double y = topLeft.y; y < topLeft.y + 3 * scale; y++)
                {
                    if (token.IsCancellationRequested)
                    {
                        busy = false; return;
                    }

                    var current = new Complex((x - canvasWidth / 2) / scale, (canvasHeight / 2 - y) / scale);
                    int i = 0;
                    for (; i < maxIterations; i++)
                    {
                        current = Complex.Pow(current, currentPower) + currentSelected;
                        if (current.Real * current.Real + current.Imaginary * current.Imaginary > 4) break;
                    }

                    if (i == maxIterations)
                        await ctx.SetFillStyleAsync("darkorange");
                    else if (i == 0) continue;
                    else if (i == 1) continue;
                    else
                        await ctx.SetFillStyleAsync(colors[i - 1]);

                    await ctx.FillRectAsync(x, y, 1, 1);
                }

                await Task.Delay(5);
            }

            busy = false;
        }

        private async Task DrawDot(Canvas2DContext ctx, double canvasX, double canvasY)
        {
            await ctx.BeginPathAsync();
            await ctx.SetFillStyleAsync("darkorange");
            await ctx.SetStrokeStyleAsync("transparent");
            await ctx.ArcAsync(canvasX, canvasY, 3, 0, 2 * Math.PI);
            await ctx.FillAsync();
        }

        private bool IsWitinBounds((double x, double y) lp) =>
            lp.x >= 0 && lp.x < canvasWidth && lp.y >= 0 && lp.y < canvasHeight;

        private bool AllWithinBounds((double x, double y) lp1, (double x, double y) lp2) =>
            IsWitinBounds(lp1) && IsWitinBounds(lp2);

        private bool TryGetIntersectionCoords(
            (double x, double y) lp1, (double x, double y) lp2,
            out double rx1, out double ry1,
            out double rx2, out double ry2)
        {
            rx1 = lp1.x; rx2 = lp2.x; ry1 = lp1.y; ry2 = lp2.y;
            var line = new LineSegment2D(new Point2D(lp1.x, lp1.y), new Point2D(lp2.x, lp2.y));
            bool r1set = false;
            for (int s = 0; s < canvasSides.Count; s++)
            {
                if (line.TryIntersect(canvasSides[s], out var i, Angle.FromDegrees(1)))
                {
                    if (!r1set)
                    {
                        r1set = true;
                        rx1 = i.X; ry1 = i.Y;
                    }
                    else
                    {
                        rx2 = i.X; ry2 = i.Y;
                        return true;
                    }
                }
            }

            if (r1set && IsWitinBounds(lp1))
            {
                rx2 = lp1.x; ry2 = lp1.y;
                return true;
            }
            else if (r1set && IsWitinBounds(lp2))
            {
                rx2 = lp2.x; ry2 = lp2.y;
                return true;
            }
            else return false;
        }
    }
}
